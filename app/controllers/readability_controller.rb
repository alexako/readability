class ReadabilityController < ApplicationController

  require 'open-uri'


  def home
  end

  def readify
    @query = params[:query]
    if @query == "" or @query == "Enter URL here"
      flash[:error] = "You need to enter something before searching"
      redirect_to :back
    else
      if @query == "demo"
        @query = Rails.root.join("public", "demo.html").to_s
      elsif !@query.starts_with?("https://") and !@query.starts_with?("http://")
        @query = "http://" + @query
      end

      begin
        source = open("#{@query}").read
        @result = Readability::Document.new(source).content.html_safe
      rescue
        @result = """\
                <h2>Oops! Something went wrong.</h2>
                <p>Make sure the URL is correct.</p>
                """.html_safe
      end

      render "result"
    end
  end
end
