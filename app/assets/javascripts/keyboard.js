$(document).on('keyup',function(evt) {
  // Open Menu with ESC key
    if (evt.keyCode == 27) {
      event.preventDefault();
      if($('.navigation-wrapper').hasClass('show-menu')) {
        close_menu();
      } else {
        open_menu();
      }
    }
});
