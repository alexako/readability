function increase_font_size() {
  var fontSize = parseInt($("#content").css("font-size"));
  fontSize += 5;
  $("#content").css({'font-size':fontSize});
}

function decrease_font_size() {
  var fontSize = parseInt($("#content").css("font-size"));
  fontSize -= 5;
  $("#content").css({'font-size':fontSize});
}

function change_font_family() {
  var fonts = [
    "sans-serif",
    "times",
    "arial",
    "calibri",
    "courier",
    "monospace"
  ];
  var current_font = $('#content').children().css('font-family').split(", ")[0];
  switch (current_font) {
    case fonts[0]: $('#content').children().css("font-family", fonts[1]); break;
    case fonts[1]: $('#content').children().css("font-family", fonts[2]); break;
    case fonts[2]: $('#content').children().css("font-family", fonts[3]); break;
    case fonts[3]: $('#content').children().css("font-family", fonts[4]); break;
    case fonts[4]: $('#content').children().css("font-family", fonts[5]); break;
    default: $('#content').children().css("font-family", fonts[0]);
  }
}

function change_color_scheme() {
  var schemes = [
    // [0] = font-color; [1] = background-color;
    [
      "rgb(0, 0, 0)",
      "rgb(255, 250, 250)"
    ],
    [
      "rgb(255, 250, 250)",
      "rgb(30, 30, 30)"
    ]
  ];

  var current_color = $("body").css("background-color");

  var option;
  if (current_color != "rgb(30, 30, 30)") {
    option = 1;
  } else {
    option = 0;
  }

  if ($("#color_toggle").hasClass("fa-toggle-off")) {
    $("#color_toggle").removeClass('fa-toggle-off');
    $("#color_toggle").addClass('fa-toggle-on');
  } else {
    $("#color_toggle").removeClass('fa-toggle-on');
    $("#color_toggle").addClass('fa-toggle-off');
  }

  $("body").css({
    'color': schemes[option][0],
    'background-color': schemes[option][1]
  });
}

function toggle_tts() {
  if ($("#tts_toggle a").hasClass("fa-toggle-on")) {
    $("#tts_toggle a").removeClass('fa-toggle-on');
    $("#tts_toggle a").addClass('fa-toggle-off');
    responsiveVoice.cancel();
  } else {
    $("#tts_toggle a").removeClass('fa-toggle-off');
    $("#tts_toggle a").addClass('fa-toggle-on');
    responsiveVoice.cancel();
    responsiveVoice.speak($('#content').text());
    $('#content').animate({scrollTop: $('#content').get(0).scrollHeight}, 50000);
  }
}

function back_to_top() {
  $("body").animate({"scrollTop": "0px"}, 700);
};
