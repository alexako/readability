function open_menu() {
  $('.navigation-wrapper').addClass('show-menu');
  $('.navigation').fadeIn();
  $('.navigation li').addClass('small-padding');
}

function close_menu() {
  $('.navigation-wrapper').removeClass('show-menu');
  $('.navigation').hide();
  $('.navigation li').removeClass('small-padding');
  close_about();
}

function open_about() {
  $('#about_details').show();
  $('.menu-item').hide();
}

function close_about() {
  $('#about_details').hide();
  $('.menu-item').show();
}

var ready;
ready = function() {
  $('#menulink').click(function(event) {
    event.preventDefault();
    if ($('.navigation-wrapper').hasClass('show-menu')) {
      close_menu();
    } else {
      open_menu();
     }
  });

  $('#about').click(function() {
    if ($('#about_details').is(':hidden')) {
      open_about();
    } else {
      close_about();
    }
  });
};


$(document).ready(ready);
$(document).on('page:load', ready);

// Open Menu if mouse enters top-left corner
$( document ).on( "mousemove", function( event ) {
  event.preventDefault();
  if (event.pageX < 20 && event.pageY < 120) {
    open_menu();
  }
});
