Rails.application.routes.draw do
  root 'readability#home'
  get 'readify', to: 'readability#readify'
  get 'result', to: 'readability#result'
end
